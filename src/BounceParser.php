<?php

namespace idfortysix\bouncenotifier;

class BounceParser
{
    private $source;
    public $reason;
    public $emailID;
    public $clientID;
    public $mailingID = '';

    public function parse(string $source)
    {
        $this->source = $source;

        $pattern = '/To\: (unsubscribe_)?([a-fA-F0-9]{32})_([a-fA-F0-9]{32})_?([a-fA-F0-9]{32})?\@/iu';
        if (preg_match($pattern, $this->source, $matches))
        {
            $this->emailID = $matches[2];
            $this->clientID = $matches[3];

            if(isset($matches[4]) && $matches[4])
            {
                $this->mailingID = $matches[4];
            }

            if(strlen($matches[1]) > 0)
            {
                // unsubscribe emailas
                $this->reason = "unsubscribe";
            }
            else
            {
                $this->reason = $this->getReason();
            }
        }
        return $this;
    }

    private function getReason()
    {
        foreach ($this->initReasons() as $reason => $keywords)
        {
            foreach ($keywords as $keyword)
            {
                if (preg_match("/".$keyword."/i", $this->source))
                {
                    return $reason;
                }
            }
        }
        return 'unknown';
    }

    private function initReasons()
    {
        return [

            'spam' => [

                'spam',
                'DNSBL listed at',
                'Blocked \(in reply to RCPT TO command\)',
                'block list',
                'Blacklisted',
                'This system is configured to reject mail',
                'listed at',
                'Message content rejected',
                'black list',
                'blocked',

            ],

            'domain_failure' => [

                'domain failure',
                'Host or domain name not found',
                'No such domain at this location',

            ],

            'server_failure' => [
                
                'server failure',
                'Command rejected (in reply to end of DATA command)',
                'Server configuration problem \(in reply to RCPT TO command\)',
                'relay not permitted \(in reply to RCPT TO command\)',
                'Relay access denied \(in reply to RCPT TO command\)',
                'Client host rejected: Access denied \(in reply to RCPT TO command\)',
                'delivery temporarily suspended',
                'authentication required',
                'can\'t create user output file',
                'Unable to relay for',
                'temporary failure',
                'This system is not configured to relay mail',
                'Connection timed out',
                'message is looping',
                'lost connection',
                'Unable to relay',
                'relaying denied',
                'CNAME lookup failed temporarily',
                'Address rejected',
                'create maildir file .+ Permission denied',
                'you are trying to use me .+ as a relay',
                'No valid hosts',
                'Unrouteable address',
                'Message can\'t be delivered',
                'no MXs for this domain could be reached at this time',
                'Requested action not taken: message refused',
                'Delivery to the following recipients has been delayed',
                'forwarded more than the maximum allowed times',
                'loops back to myself',
                'no RSET response',
                'Donatas Akstinas \(in reply to end of DATA command\)',
                'Delayed Mail \(still being retried\)',
                'Sender denied',
                'mailbox not available',
                'recipient rejected \(in reply to RCPT TO command\)',
                'Malformed or unexpected name server reply',
                'I have been attempting to forward the mail',
                'Sorry\.+ \(in reply to MAIL FROM command\)', 
                'said\: 550 \(in reply to RCPT TO command\)',
                'The mail system \<[0-9a-zA-Z\_\-]+\@matrix\.lt\>\: matrix\.lt',
                'said\: 550 \" \(in reply to RCPT TO command\)',
                'unable to connect successfully',
                'Syntax error in parameters',
                'said\: 553 RP\:RDN',
                'Timeout \- psmtp \(in reply to end of DATA command\)',
                'mail transport unavailable',
                'Transaction Failed',
                'DELFI pa', 
                
            ],

            'over_quota' => [
    
                'over quota',
                'Storage quota exceeded',
                'maildir over quota',
                'space quota exceeded',
                'Over quota \(in reply to RCPT TO command\)',
                'can not open new email file errno',
                'mailbox is full',
                'the user\'s maildir has overdrawn his diskspace quota',
                'Mailbox quota exceeded',
                'user is over quota',
                'user over quota',
                'Mailbox full \(in reply to end of DATA command\)',
                'Mail quota exceeded',
                'Mailbox size exceeded',
                'space exceeded',
                'quota exceeded',
                'enough space',
                'Mailbox full',
                'over quota',
                'maildir delivery failed\: Full',
                'User has too many messages on the server',
                'File too large',
                'exceeded',
                'Inbox is full',
                'kvotos limitas',
                'exhausted',
                'contains too much data',
                'quota',
            
            ],
            
            'unknown_user' => [
            
                'unknown user',
                'User unknown in virtual mailbox table',
                'Unknown user \(in reply to RCPT TO command\)',
                'User unknown \(in reply to RCPT TO command\)',
                'Sorry\, no mailbox here by that name',
                'Recipient address rejected\:',
                'mailbox unavailable \(in reply to RCPT TO command\)',
                'The email account that you tried to reach does not exist',
                'unknown user',
                'No Such User Here \(in reply to RCPT TO command\)',
                'user does not exist',
                'Mailbox .+ does not exist',
                'invalid address \(in reply to RCPT TO command\)',
                'Delivery to the following recipients failed',
                'Delivery to the following recipient failed permanently',
                'Unknown recipient',
                'User unknown',
                'Adresatas neegzistuoja',
                'Message rejected \(no valid recipients\)',
                'Recipient not found',
                'mailbox unavailable',
                'Sis el\. pastas nebeegzistuoja\.',
                'Recipient unknown',
                'The following address\(es\) failed',
                'Delivery not authorized\, message refused',
                'Recipient.address.rejected',
                'Invalid recipient',
                'Name or service not known',
                'is not one of our own addresses or at least syntactically incorrect',
                'address .+ was erased', 
                'User is unknown',
                'Address not present',
                'No such person',
                'User not found',
                'No such user',
                'This user .+ have .+ account',
                'User .+ unknown',
                'recipient .+ was not found',
                'did not reach the following recipient',
                'user invalid',
                'Permanent Failure Address Status Bad destination mailbox address',
                'No such recipient',
                'User gone',
                'Subject\: Klaidingas el\.',
                'Invalid email address',
                'No such email box',
                'adresas daugiau neveikia',
                'not a known user',
                'Bad destination mailbox address',
                'Recipient not recognized',
                'Invalid Mailbox',
                'Address does not exist',
                'Unknown local part',
                'unknown or illegal alias',
                'No mail box available for this user',
                'Unknown account',
                'Pasikeit.+el.pa.+to.adresas',
                'No such mailbox',
                'Recipient rejected\. \(in reply to RCPT TO command\)',
            
            ],
            
            'user_disabled' => [
                
                'user disabled',
                'account has been suspended',
                'account disabled',
                'account is disabled',
                'Account inactive',
                'user disabled',
                'The email account that you tried to reach is disabled',
                'This account has been disabled',
                'Mailbox disabled for this recipient',
                'This address no longer accepts mail',
            
            ],
            
            'vacation' => [
            
                'vacation',
                'I will not be reading my mail on',
                'iuo.metu.atostogauju',
                'I am currently out until',
                'out of the office',
                'Out.of.office',
                'away from the office',
                'esu isvyk.s',
                'Atostogos',
                'in the office',
                'out of office',
            
            ],
            
            'access_deny' => [
            
                'access deny',
                'not have permission to post messages',
                'do not have SMTP Authentication turned on',
                'Local Policy Violation',
                'No relaying allowed',
                'Denied by policy',
                'security policies',
                'rejected for policy reasons',
                'Message rejected',
                'Denied by policy \(in reply to end of DATA command\)',
                'Access denied',
                'Failure From and Envelope from must both be local domains add',
                'sorry\, that domain isn\'t in my list of allowed rcpthosts',
                'Mail appears to be unsolicited',
                'Account is not local\, relaying disallowed\.',
                'server requires authentication',
                'validrcptto list',
                'Diagnostic\-Code\: smtp\, 550 Address unknown',
                'Administrative prohibition',
            
            ],
            
            'auto_reply' => [
            
                'auto reply',
                'Subject\:.{1,50}Re\:',
                'no longer working at the',
                'Subject\: Autoreply\:',
                'Subject\: \[Auto\-Reply\]',
                'automaatvastus',
                'Thank you for your e\-mail',
                'This message was automatically generated by email software',
                'I will be back to work',
                'X\-Mailer\: The Bat\!',
                'Subject\: Re\: ',
            
            ]
        ];

    }

}