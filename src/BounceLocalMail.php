<?php

namespace idfortysix\bouncenotifier;

use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;

class BounceLocalMail
{
    // config vars
    private $local_dirs;
    private $max_bounce_get;

    public function __construct()
    {
        $this->max_bounce_get   = function_exists('config') ? config('services.dispatch.max_bounce_get') : $_ENV['MAX_BOUNCE_GET'];
        $this->local_dirs       = explode(',', function_exists('config') ? config('services.dispatch.local_mail_dirs') : $_ENV['LOCAL_MAIL_DIRS'] );
    }

    public function run()
    {
        $notifier = new BounceNotifier;
        $parser = new BounceParser;

		foreach($this->local_dirs as $dir)
		{
			if (!file_exists($dir))
			{
				continue;
			}
            // ++ pakartojimu ciklas env kintamasis

            $bounce_get_i = 0;

            foreach ($iterator = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($dir, 
                    RecursiveDirectoryIterator::SKIP_DOTS),
                RecursiveIteratorIterator::SELF_FIRST) as $item)
            {
                if($item->isFile())
                {
                    $bounce_get_i++;
                }
                if($bounce_get_i > $this->max_bounce_get)
                {
                    break;
                }

                $file = $item->getRealPath();

                if (!file_exists($file) || $item->isDir())
                {
                    continue;
                }

                echo $file . "\n";

                $parser->parse( file_get_contents($file) );

                // grupuojame pagal clientID
                // suformuojame Bounce Array
                $notifier->addBounceSet($parser->clientID, $parser->emailID, $parser->mailingID, $parser->reason);

                unlink($file);
            }

        }
        
        if ($notifier->notifySendapiBounce())
        {
            echo "SECONDCHECK:". (time() + 3600) ."\n";
        }
        else
        {
            echo "bounce failed\n";
        }
    }
    
}