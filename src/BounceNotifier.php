<?php

namespace idfortysix\bouncenotifier;

use GuzzleHttp\Client as GuzzleClient;

class BounceNotifier
{
    private $bounce_arr;
    private $sendapi_url;
    private $sendapi_apikey;

    public function __construct()
    {
        $this->bounce_arr = [];
        $this->sendapi_url      = function_exists('config') ? config('services.dispatch.track_url') : $_ENV['TRACK_URL'];
        $this->sendapi_apikey   = function_exists('config') ? config('services.dispatch.track_key') : $_ENV['TRACK_KEY'];
    }

    public function addBounceSet($clientID, $emailID, $mailingID, $reason)
    {
        if($clientID)
        {
            $this->bounce_arr[$clientID][] = [
                'e' => $emailID,
                'm' => $mailingID,
                'r' => $reason
            ];
        }
    }

    /**
     * notifikuojame sendapi per Http guzzle
     */
    public function notifySendapiBounce()
    {
        $success = true;
        if (!$this->bounce_arr)
        {
            return true;
        }

		foreach ($this->bounce_arr as $client_id => $json)
		{
			$url = $this->sendapi_url . "b/$client_id/";

			try {
				$response = (new GuzzleClient)
					->request('POST', $url, [
					'headers' => [
						'apikey' => $this->sendapi_apikey
					],
					'json' => $json
                ]);
                
                $response_json = json_decode($response->getBody());

                $result = isset($response_json->status) ? $response_json->status : "ERROR";
    
                if ($result == 'OK')
                {
                    echo "bounced: $client_id count:".$response_json->count."\n";
                }
                else
                {
                    echo "Error to post bounces to: $url $client_id - $result\n";
                    $success = false;
                }
            }
			catch (\Exception $e)
			{
				$success = $this->catchErrors($e, $url, $json);
			}

        }
        // grazinam ar nebuvo klaidu perduodant Bouncus
        return $success;
    }

    /*
	 * Funkcija skirta konsoleje atvaizduoti errora
	 */
    private function catchErrors($e, $command, $json)
    {
        $debug_body = var_dump($command, $json);
        echo "\n$debug_body";
        echo "\n".$e->getMessage();
        echo "\n".$e->getTraceAsString()."\n\n";

        return false;
    }

}