<?php
namespace idfortysix\bouncenotifier;

require("src/BounceLocalMail.php");
require("src/BounceNotifier.php");
require("src/BounceParser.php");

$_ENV['MAX_BOUNCE_GET'] = 10;
$_ENV['LOCAL_MAIL_DIRS'] = "/tmp";

$local_runner = new BounceLocalMail();
$local_runner->run();

